/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
	theme: {
		extend: {
			fontFamily: {
				akzidenzgrotesk: ['akzidenzgrotesk', 'sans-serif']
			},
			colors: {
				primary: {
					DEFAULT: '#DFE0D9',
					light: '#CFD1C7',
					blue: '#293894',
					'blue-transparent': 'rgba(41, 56, 148, 0.9)',
					'black-transparent': 'rgba(27, 27, 27, 0.9)',
					black: '#1B1B1B',
					red: '#E94235'
				}
			},
			spacing: {
				'7': '2.1rem',
				'14': '3.5rem',
				'17': '6.5rem',
				'34': '8.75rem',
				'42': '11.6rem'
			},
			lineHeight: {
				'extra-loose': '2.75rem'
			},
			letterSpacing: {
				tightest: '-.18rem',
				widest: '.25em'
			}
		}
	},
	variants: {},
	plugins: []
}
